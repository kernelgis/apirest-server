const express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express();

/* *******************************************************************  */
// Settings


/* *******************************************************************  */
// Middleware
app.use(cors())
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())


/* *******************************************************************  */
// Routes
app.use('/', require('./src/routes/index_rout'));


/* *******************************************************************  */
// Start Server
app.listen(3000, () => {
  console.log(`Servidor inicializado en el puerto 3000`);
})


