const pool = require('./connection_database');

async function getAllQry() {
  try {
    // const sql = `select idUsuario, nombre, apellidos, edad, username, activo from usuarios`;
    const sql = `call sp_getAllUsers`;
    const [rows] = await pool.query(sql);
    return rows;
  } catch (error) {
    console.error('Error', error);
    throw error;
  }
}

async function newUsuarioQry(data) {
  try {
    const sql = `insert into usuarios set ?`;
    const [rows] = await pool.query(sql, [data]);
    return rows;
  } catch (error) {
    console.error('Error', error);
    throw error;
    
  }
}

async function updateUsuarioByIDQry(id, data) {
  try {
    const sql = `update usuarios set ? where idUsuario = ?`;
    const [rows] = await pool.query(sql, [data, id]);
    return rows;
  } catch (error) {
    console.error('Error', error);
    throw error;
    
  }
}

module.exports = {
  getAllQry,
  newUsuarioQry,
  updateUsuarioByIDQry
}