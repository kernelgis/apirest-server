const mysql = require('mysql2');
const { credencialesDB } = require('./config/credenciales_db');

// crear la Conexión a la base de Datos.

const pool = mysql.createPool(credencialesDB);
const poolPromise = pool.promise();

module.exports = poolPromise;



