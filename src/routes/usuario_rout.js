const express = require('express');
const router = express.Router();

const { 
  getAllCtrl,
  getUsuarioByIDCtrl,
  updateUsuarioByIDCtrl,
  newUsuarioCtrl,
  deleteUsuarioCtrl
 } = require('../controllers/usuario_ctrl');
const app = require('./index_rout');

router.route('/')
  .get(getAllCtrl);

router.route('/:idUsuario')
  .get(getUsuarioByIDCtrl)
  .put(updateUsuarioByIDCtrl)
  .delete(deleteUsuarioCtrl);

router.route('/nuevo-usuario')
  .post(newUsuarioCtrl);
module.exports = router;
