const express = require('express');
const app = express();

// Instacias de los Routes
const usarioRoute = require('./usuario_rout');
const motoRoute = require('./moto_rout');
const mttoRoute = require('./mtto_rout');

// Routes
app.use('/usuario', usarioRoute);
app.use('/moto', motoRoute);
app.use('/mantenimiento', mttoRoute);

module.exports = app;