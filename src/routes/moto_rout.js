const express = require('express');
const router = express.Router();
const { getAllCtrl } = require('../controllers/moto_ctrl')

router.route('/')
  .get(getAllCtrl);

module.exports = router;