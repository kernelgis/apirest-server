
const { 
  getAllQry,
  newUsuarioQry, 
  updateUsuarioByIDQry
} = require('../models/usuario_qry')

module.exports = {
  getAllCtrl: (req, res) => {

    getAllQry()
      .then(result => {
        res.json(result);
      })
      .catch(error => {
        res.json(error);
      });



  },
  getUsuarioByIDCtrl: (req, res) => {

    const { idUsuario } = req.params;
    let respuesta = {
      succes: true,
      data: `Usuario con id: ${idUsuario} encontrado`
    }
    res.json(respuesta);
  },
  updateUsuarioByIDCtrl: (req, res) => {

    const id = req.params.idUsuario;

    const data = {
      idUsuario, nombre, apellidos, edad,
      username, password, activo
    } = req.body;

    updateUsuarioByIDQry(id, data)
      .then(result => {
        res.json(result);
      })
      .catch(error => {
        res.json(error);
      });
  },
  deleteUsuarioCtrl: (req, res) => {
    const { idUsuario } = req.params;
    let respuesta = {
      succes: true,
      data: `Usuario con id: ${idUsuario} ha sido borrado`
    }
    res.json(respuesta);

  },
  newUsuarioCtrl: (req, res) => {

    // console.log(req.body);
    const data = {
      idUsuario, nombre, apellidos, edad,
      username, password, activo
    } = req.body;

    newUsuarioQry(data)
      .then(result => {
        res.json(result);
      })
      .catch(error => {
        res.json(error);
      });
  }


}
